# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the fcitx5-rime package.
#
# Translators:
# csslayer <wengxt@gmail.com>, 2017
# Mingcong Bai <jeffbai@aosc.xyz>, 2018
msgid ""
msgstr ""
"Project-Id-Version: fcitx5-rime\n"
"Report-Msgid-Bugs-To: fcitx-dev@googlegroups.com\n"
"POT-Creation-Date: 2018-02-06 00:01-0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Mingcong Bai <jeffbai@aosc.xyz>, 2018\n"
"Language-Team: Chinese (China) (https://www.transifex.com/fcitx/teams/12005/"
"zh_CN/)\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: src/rimeengine.cpp:106
msgid "Deploy"
msgstr "重新部署"

#: src/rimeengine.cpp:237 src/rime.conf.in:3 src/rime-addon.conf.in:3
msgid "Rime"
msgstr "中州韻"

#: src/rime-addon.conf.in:4
msgid "Rime Wrapper For Fcitx"
msgstr "Fcitx 的中州韻封装"

#: src/rimeengine.cpp:229
msgid "Rime has encountered an error. See /tmp/rime.fcitx.ERROR for details."
msgstr "Rime 出现了一个错误。请参阅/tmp/rime.fcitx.ERROR中细节。"

#: src/rimeengine.cpp:227
msgid "Rime is ready."
msgstr "Rime 就绪。"

#: src/rimeengine.cpp:225
msgid "Rime is under maintenance ..."
msgstr "Rime 正在维护中..."

#: src/rimeengine.cpp:125
msgid "Synchronize"
msgstr "同步"

#: src/rimeengine.cpp:124
msgid "rime-sync"
msgstr "rime-sync"
